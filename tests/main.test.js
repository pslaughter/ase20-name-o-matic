import { getIndexHtmlBody } from "./fixtures";

describe("main", () => {
  // helper functions -------
  const runSubject = () => {
    require("../public/main");

    // Reset modules so we can require this again without it being cached
    jest.resetModules();
  };

  const findButton = () => document.getElementById("js-button");
  const findName = () => document.getElementById("js-name");
  const findTextarea = () => document.getElementById("js-textarea");
  const isShowingLoader = () => Boolean(findName().querySelector("svg"));

  // tests -------
  beforeEach(() => {
    document.body.innerHTML = getIndexHtmlBody();

    // lets enforce some determinism
    jest.spyOn(Math, "random").mockReturnValue(0.4);
    jest.useFakeTimers();

    runSubject();
  });

  it("does not show loading", () => {
    expect(isShowingLoader()).toBe(false);
  });

  describe("when button is pressed", () => {
    beforeEach(() => {
      findButton().dispatchEvent(new Event("click"));
    });

    it("shows loading", () => {
      expect(isShowingLoader()).toBe(true);
    });

    describe("after loading", () => {
      beforeEach(() => {
        // Executes whatever has been passed to `setTimeout` or `setInterval`
        jest.runOnlyPendingTimers();
      });

      it("shows random name", () => {
        expect(findName().textContent).toContain("Emma");
      });
    });
  });
});
